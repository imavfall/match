const { DataTypes, Op } = require("sequelize");
const sequelize = require('../config/dev');
const like = require('../models/Like.js');
const error_handling = require('../tools/error_handling.js');

const likeInstance = like(sequelize, DataTypes);

exports.list_user_like = async function(req, res) {
  try {
    console.log(req.params);
    var like = await likeInstance.findAll({ 
      attributes: [['sender', 'user_id'], 'status'],
      offset: req.params.skip || 0, 
      limit: (req.params.limit > 50 ? 50: req.params.limit),
      where: {
        user_id: req.params.user_id
      }
    })
    res.json(like);
  } catch (e) {
    error_handling(e, res)
  }
};

exports.post_a_like = async function(req, res) {
  try {
    req.body.sender = req.body.user_id;
    req.body.user_id = parseInt(req.params.user_id, 10);
    var like = await likeInstance.create(req.body);
    res.json(like);
  } catch (e) {
    error_handling(e, res);
  }
};


